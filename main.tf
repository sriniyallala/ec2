provider "aws" {
  region = "us-west-2"
}

module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "my-cluster"
  instance_count         = 3

  ami                    = "ami-02f147dfb8be58a10"
  instance_type          = "t2.micro"
  key_name               = "bamboo"
  monitoring             = true
  vpc_security_group_ids = ["sg-9aef18c8"]
  subnet_id              = "subnet-1dbf7d65"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}